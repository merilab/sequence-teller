require './lib/sequence_sanitizer'
require './lib/sequence_teller'

puts 'Enter a sequence of numbers separated by commas (i.e.: 1,2,3,4,5):'
raw_sequence = gets.chomp
sanitized_sequence = SequenceSanitizer.new(input: raw_sequence).call

if sanitized_sequence
  next_number = SequenceTeller.new(sequence: sanitized_sequence).call

  puts next_number
end

# sequence-teller

# Use

1. Clone the repo

```
$ git clone
```

2. Install the dependencies:

```
$ bundle install
```

3. Run the script:

```
$ ruby start.rb
```

4. Follow the prompted instructions.

# Contribute

1. Clone the repo

```
$ git clone
```

2. Create a new branch

```
$ git checkout -b my-new-branch
```

3. Make your changes and use descriptive commit messages
4. Open a PR to request a review
5. Once the review is passed, the PR will be merged.

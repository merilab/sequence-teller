require './lib/sequence_teller'

RSpec.describe SequenceTeller do
  it 'solves sequences with consecutive trend' do
    assert_this([1, 2, 3, 4], 5)
    assert_this([2, 4, 6, 8], 10)
    assert_this([3, 2, 1, 0, -1], -2)
    assert_this([50, 40, 30], 20)
    assert_this([5, 6, 17, 48, 109], 210)
    assert_this([2, 92, 175, 250, 316], 372)
  end

  it 'solves sequences with non consecutive trend' do
    assert_this([4, 9, 29], 34)
    assert_this([4, 9, 29, 34], 54)
    assert_this([4, 9, 29, 34, 54], 59)
    assert_this([4, 9, 29, 34, 54, 59], 79)
    assert_this([4, 9, -11, -6, -26], -21)
    assert_this([7, 11, 20, 9, 3], -23)
    assert_this([8, 5, 15, 12], 22)
  end

  def assert_this(sequence, next_expected_number)
    result = described_class.new(sequence: sequence).call

    expect(result).to eq(next_expected_number)
  end
end

require './lib/sequence_sanitizer'

RSpec.describe SequenceSanitizer do
  it 'shows error messages for invalid sequences' do
    expect do
      described_class.new(input: '2').call
    end.to output("Invalid sequence.\n").to_stdout

    expect do
      described_class.new(input: nil).call
    end.to output("Invalid sequence.\n").to_stdout
  end

  it 'returns false for invalid sequences' do
    assert_this(nil, false)
    assert_this('Hello', false)
    assert_this('1', false)
  end

  it 'sanitizes the received input' do
    assert_this('1,2,3,4', [1, 2, 3, 4])
    assert_this('1, 2, 3, 4', [1, 2, 3, 4])
  end

  def assert_this(input, expected_sequence)
    sanitized_input = described_class.new(input: input).call
    expect(sanitized_input).to eq(expected_sequence)
  end
end

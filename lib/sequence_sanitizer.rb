class SequenceSanitizer
  def initialize(input:)
    @input = input
  end

  def call
    sanitized = @input.delete(' ').split(',')
    sequence = sanitized.map(&:to_i)

    if sequence.count <= 1
      puts 'Invalid sequence.'
      false
    else
      sequence
    end
  rescue Exception
    puts 'Invalid sequence.'
    false
  end
end

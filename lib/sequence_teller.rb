class SequenceTeller
  def initialize(sequence:)
    @sequence = sequence
  end

  def call
    extract_sequence(@sequence)
  end

  private

  def consecutive_sequence?(trend)
    trend.uniq.size <= 1
  end

  def extract_sequence(sequence)
    sequence.last + next_difference(extract_trend(sequence))
  end

  def extract_trend(sequence)
    sequence.each_with_index.filter_map do |number, index|
      next_number = sequence[index + 1]
      next_number.nil? ? next : next_number - number
    end
  end

  def next_difference(trend)
    return trend.first if non_consecutive_squence?(trend)
    return trend.last if consecutive_sequence?(trend)

    extract_sequence(trend)
  end

  def non_consecutive_squence?(trend)
    trend.each_slice(2).uniq.size <= 1
  end
end
